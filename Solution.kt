class Solution {
    fun canCompleteCircuit(gas: IntArray, cost: IntArray): Int {
        var answer = -1
        val size = gas.size
            for(start in 0..size-1) {
                var tank = 0
                tank = tank + gas[start]
                // println("start at station ${start} (index ${start}) and fill up with ${gas[start]}. Your tank = 0 + ${gas[start]} = $tank")
                for(station in 0..size-1) {
                    // println("Travel to station ${(start+station+1)%size}")
                    // println("Your tank = $tank - ${cost[(start+station)%size]} = ${(tank - cost[(start+station)%size])}")
                    tank = (tank - cost[(start+station)%size])
                    if(tank < 0) {
                        break
                    } else {
                        // println("Fill tank = $tank + ${gas[(start+station+1)%size]} = ${tank + gas[(start+station+1)%size]}")
                        tank = tank + gas[(start+station+1)%size]
                    }
                }
                if(tank >= 0) {
                    answer = start
                    break
                } 
            }
        return answer
    }
}